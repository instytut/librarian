# Change Log

This document records all notable changes to Redakcja.

## 1.7 (2018-03-26)

### Added
- Python 3.4+ support, to existing Python 2.7 support.
- `from_bytes` methods on all classes which had `from_string` methods.
  The latter are now deprecated and provide transitional support
  for current inconsitent use of byte strings and text inputs.
- `OutputFile.get_bytes`, deprecating `OutputFile.get_string`.
- This change log.

### Fixed
- Packaging.
- Tests: all are now working, and configured for `tox`.
- `font-optimizer`for Perl 5.22.

